# -*- coding: utf-8 -*-
# Copyright (C) 2013 by Mattia Monga <mattia.monga@unimi.it> 
# This program is free software, licensed under GPLv3, see README.md

import os

DB_HOST = os.environ.get('OPENSHIFT_POSTGRESQL_DB_HOST')
DB_PORT = os.environ.get('OPENSHIFT_POSTGRESQL_DB_PORT')
APP_NAME = os.environ.get('OPENSHIFT_APP_NAME')
DB_USER = os.environ.get('OPENSHIFT_POSTGRESQL_DB_USERNAME')
DB_PASSWORD = os.environ.get('OPENSHIFT_POSTGRESQL_DB_PASSWORD')
DB_CONNECTION = os.environ.get('OPENSHIFT_POSTGRESQL_DB_URL') + '/' + APP_NAME
DB_TABLE = 'libri3'


PY_DIR = os.path.join(os.environ['OPENSHIFT_HOMEDIR'], "python")
STATIC_DIR = os.path.join(os.environ['OPENSHIFT_REPO_DIR'], 'wsgi', 'static')
DEBUG=False

class AppStatus:
    DOWN = 0
    COLLECT = 1
    DONE = 2

APP_STATUS = AppStatus.DONE
