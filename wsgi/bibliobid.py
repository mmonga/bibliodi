# -*- coding: utf-8 -*-
# Copyright (C) 2013 by Mattia Monga <mattia.monga@unimi.it> 
# This program is free software, licensed under GPLv3, see README.md

import re, os, csv
from flask import g, Flask, render_template, send_from_directory, \
    request, Response
from sqlalchemy import create_engine, MetaData, Table
from sqlalchemy.sql import select, or_, and_, update
from hashlib import sha1

import config

CACHE = {}
CATS = []

app = Flask(__name__)

#  Colonna |  Tipo   | Modificatori 
# ---------+---------+--------------
#  anno    | text    | 
#  titolo  | text    | 
#  autori  | text    | 
#  coll    | text    | 
#  num     | text    | 
#  tenere  | integer | 
def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        g._database = create_engine(config.DB_CONNECTION)
        g._meta = MetaData()
        g.libri = Table(config.DB_TABLE, g._meta, autoload=True, 
                        autoload_with=g._database)
    return g._database

def mightbe_none(field):
    t = u''
    if field: t = u'' + field
    return t

def get_cats():
    t = []
    c = get_db()
    r = c.execute(select([g.libri.c.coll]).distinct().\
                  order_by(g.libri.c.coll))
    t = r.fetchall()
    r.close()
    return t
    
@app.route('/sort/<key>/dir/<direction>/cat/<category>/\
filter/<pattern>/max/<int:maxres>')
def isorted(key=None, direction='desc', pattern='', maxres=1000, category=''):
    global CATS, CACHE
    if key != 'anno' and key != 'autori' and \
       key != 'titolo' and key != 'tenere':
        key = 'anno'
    odir = 'asc'
    c = get_db()
    fdirection = g.libri.c[key].desc
    if direction != 'desc':
        direction = 'asc'
        fdirection = g.libri.c[key].asc
        odir = 'desc'
    if not CATS: CATS = [i[0] for i in get_cats() if i[0]]
    if unicode(category) not in CATS:
        category = '%_%'
    if not re.match('\w+',pattern): pattern = '%_%'
    else: pattern = '%' + pattern + '%'
    if maxres < 1: maxres = 1000
    q = c.execute(select([g.libri.c.anno, 
                          g.libri.c.autori, 
                          g.libri.c.titolo, 
                          g.libri.c.tenere, 
                          g.libri.c.coll]).\
                  where(and_(or_(g.libri.c.autori.ilike(pattern),
                                 g.libri.c.titolo.ilike(pattern)), 
                             g.libri.c.coll.ilike(category))).\
                        limit(maxres).order_by(fdirection()))
    d = []
    for r in q:
        if r[1] or r[2]:
            h = sha1((u'salt' + 
                      r[0] + r[1] + 
                      r[2] + r[4]).encode('utf-8',\
                                          errors='ignore')).hexdigest()
            d += [(h, r[0], r[1], r[2], bool(r[3]), r[4])]
            CACHE[h] = (r[0], r[1], r[2], r[4])

    q.close()
    return render_template('index.html', 
                           data=d, odirection=odir, mres=maxres, 
                           cat=category.strip('%'), categories=CATS, 
                           filter=pattern.strip('%'))

@app.route('/')
def index():
    if config.APP_STATUS == config.AppStatus.COLLECT:
        return isorted('anno')
    if config.APP_STATUS == config.AppStatus.DOWN:
        return render_template('index-down.html')
    if config.APP_STATUS == config.AppStatus.DONE:
        return render_template('index-done.html', words=word_count())
    assert(False)


def word_count():
    WORD = re.compile('[a-zA-Z]+')
    STOP_WORDS = re.compile("^(i|me|my|myself|we|us|our|ours|ourselves|you\
|your|yours|yourself|yourselves|he|him|his|himself|she|her|hers|herself|it\
|its|itself|they|them|their|theirs|themselves|what|which|who|whom|whose|this\
|that|these|those|am|is|are|was|were|be|been|being|have|has|had|having|do\
|does|did|doing|will|would|should|can|could|ought|i'm|you're|he's|she's|it's\
|we're|they're|i've|you've|we've|they've|i'd|you'd|he'd|she'd|we'd|they'd\
|i'll|you'll|he'll|she'll|we'll|they'll|isn't|aren't|wasn't|weren't|hasn't\
|haven't|hadn't|doesn't|don't|didn't|won't|wouldn't|shan't|shouldn't|can't\
|cannot|couldn't|mustn't|let's|that's|who's|what's|here's|there's|when's\
|where's|why's|how's|a|an|the|and|but|if|or|because|as|until|while|of|at|by\
|for|with|about|against|between|into|through|during|before|after|above|below\
|to|from|up|upon|down|in|out|on|off|over|under|again|further|then|once|here\
|there|when|where|why|how|all|any|both|each|few|more|most|other|some|such|no\
|nor|not|only|own|same|so|than|too|very|say|says|said|shall|e|using|per|una\
|un|non|il|le|la|al|del|dell'|della|di|dell|et|l|de|use|alla|carlo|allo\
|delle|degli|des|schaum)$")
    tenere = {} 
    c = get_db()
    q = c.execute(select([g.libri.c.anno, 
                          g.libri.c.titolo, 
                          g.libri.c.autori, 
                          g.libri.c.coll, 
                          g.libri.c.num]).\
                  where(g.libri.c.tenere == 1))
    for t in q:
        for w in t[1].split(): # titolo
            m = WORD.match(w.strip().lower())
            if m and not STOP_WORDS.match(m.group()):
                tenere.setdefault(m.group(),0) 
                tenere[m.group()] += 1 
    q.close()
    q = c.execute(select([g.libri.c.titolo]).\
                  where(g.libri.c.tenere == 0))
    for t in q:
        for w in t[0].split(): # titolo
            m = WORD.match(w.strip().lower())
            if m and m.group in tenere:
                tenere[m.group()] -= 1 
    q.close()

    tags = {k: v for k,v in tenere.iteritems() if v > 1}
    r = u''
    for k,v in tags.iteritems():
        r += u'{ text:"' + k + u'",size:' \
             + unicode(str(int(1.7**(v**.5)))) + u'},'
    return '[' + r + ']'

@app.route('/lista.csv')
def create_csv():
    c = get_db()
    q = c.execute(select([g.libri.c.anno, 
                          g.libri.c.titolo, 
                          g.libri.c.autori, 
                          g.libri.c.coll, 
                          g.libri.c.num]).\
                  where(g.libri.c.tenere == 1))
    def generate(q):
        yield 'ANNO, TITOLO, AUTORI/CURATORI, COLLOCAZIONE, NUM\n'
        for t in q:
            yield ','.join(['"%s"' % \
                            f.encode('utf-8').replace('"','\\"') \
                            for f in t]) + '\n'
    return Response(generate(q), mimetype='text/csv')

@app.route('/set')
def setflag():
    if config.APP_STATUS != config.AppStatus.COLLECT:
        return ""
    global CACHE
    h = request.args.get('hash', '')
    if h:
        h = h[len('link'):]
        if h in CACHE: 
            c = get_db()
            q = update(g.libri).\
                where(and_(g.libri.c.anno == CACHE[h][0],
                           g.libri.c.autori == CACHE[h][1],
                           g.libri.c.titolo == CACHE[h][2],
                           g.libri.c.coll == CACHE[h][3])).\
                values(tenere=1)
            c.execute(q)
            return ""
    assert(False)

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(config.STATIC_DIR, 
                               'favicon.ico', 
                               mimetype='image/vnd.microsoft.icon')


@app.teardown_request
def teardown_request(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        g._database = None

@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        g._database = None


if __name__ == '__main__':
    app.run(debug=config.DEBUG)
