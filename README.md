A web application for bidding the books of a library. 

It is a very basic application running on Flask
(<http://flask.pocoo.org>) and the RedHat OpenShift PaaS.

The OpenShift `python` cartridge documentation can be found at:

<https://github.com/openshift/origin-server/tree/master/cartridges/openshift-origin-cartridge-python/README.md>

It relies on `Postgres 9.2` cartridge for persistency.

# License #

*Copyright (C) 2013 by Mattia Monga <mattia.monga@unimi.it>*

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

