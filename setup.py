from setuptools import setup

setup(name='BiblioBID',
      version='1.0',
      description='BiblioBID OpenShift App',
      author='Mattia Monga',
      author_email='mattia.monga@unimi.it',
      url='http://www.python.org/sigs/distutils-sig/',
      install_requires=['Flask','SQLAlchemy'],
     )
